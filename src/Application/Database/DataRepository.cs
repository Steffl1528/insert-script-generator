using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LocalScriptGenerator.Database;

public interface IDataRepository
{
	string[] GetTableColumns(string tableName);
	IEnumerable<dynamic> GetStudentDataFromGivenTable(string[] columnNames, string tableName);
	IEnumerable<dynamic> GetSchoolDataFromGivenTable(string[] columnNames, string tableName);
}

public class DataRepository : IDataRepository
{
	private readonly ISqlConnectionFactory _db;

	public DataRepository(ISqlConnectionFactory db)
	{
		_db = db;
	}

	public string[] GetTableColumns(string tableName)
	{
		try
		{
			using var connection = _db.CreateConnection();
			var columns = connection.Query<string>(SQL.GetTableColumns,
				new { TableName = tableName }).ToArray();

			if (columns == null || !columns.Any())
			{
				throw new Exception("No Data was found.");
			}

			return columns!;
		}
		catch (Exception e)
		{
			throw LogErrorAndReturnException(SQL.GetTableColumns, e);
		}
	}


	public IEnumerable<dynamic> GetStudentDataFromGivenTable(string[] columnNames, string tableName)
	{
		var query = SQL.GetSelectStatement(columnNames, tableName);

		try
		{
			using var connection = _db.CreateConnection();
			var result = connection.Query(query).ToList();

			if (result == null || !result.Any())
			{
				throw new Exception("No Data was found.");
			}

			return result;
		}
		catch (Exception e)
		{
			throw LogErrorAndReturnException(query, e);
		}
	}

	public IEnumerable<dynamic> GetSchoolDataFromGivenTable(string[] columnNames, string tableName)
	{
		var query = SQL.GetSelectStatementForSchoolData(columnNames, tableName);

		try
		{
			using var connection = _db.CreateConnection();
			var result = connection.Query(query).ToList();

			if (result == null || !result.Any())
			{
				throw new Exception("No Data was found.");
			}

			return result;
		}
		catch (Exception e)
		{
			throw LogErrorAndReturnException(query, e);
		}
	}

	private static Exception LogErrorAndReturnException(string query, Exception? e = null)
	{
		Console.WriteLine("ERROR occurred  while executing: " + query);

		if (e != null)
		{
			Console.WriteLine(e);
		}

		return new Exception();
	}
}

public static class SQL
{
	public const string GetTableColumns = @"SELECT COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TableName
ORDER BY COLUMN_NAME;";

	public static string GetSelectStatementForSchoolData(string[] columnNames, string tableName)
	{
		var sb = CreateStringBuilderWithSelectAndFilledColumns(columnNames, tableName, true);

		sb.Append(@$" FROM Student student
JOIN {tableName} {tableName.ToLower()}
ON student.SchoolId = {tableName.ToLower()}.Id
WHERE student.Age < 10");
		return sb.ToString();
	}

	public static string GetSelectStatement(string[] columnNames, string tableName)
	{
		var sb = CreateStringBuilderWithSelectAndFilledColumns(columnNames, tableName);

		sb.Append(@$" FROM {tableName}");
		return sb.ToString();
	}

	private static StringBuilder CreateStringBuilderWithSelectAndFilledColumns(string[] columnNames, string? tableName, bool distinct = false)
	{
		var sb = new StringBuilder("SELECT ");
		if (distinct)
		{
			sb.Append("DISTINCT ");
		}

		sb.AppendJoin(",",
			string.IsNullOrEmpty(tableName)
				? columnNames
				: columnNames.Select(c => string.Concat($"{tableName.ToLower()}.", c)));

		return sb;
	}
}