﻿using System.Data;
using System.Data.SqlClient;

namespace LocalScriptGenerator.Database
{
	public class SqlConnectionFactory : ISqlConnectionFactory
	{
		private readonly string _connectionString;
		public SqlConnectionFactory(string connectionString)
		{
			_connectionString = connectionString;
		}

		public IDbConnection CreateConnection()
		{
			var connection = new SqlConnection(_connectionString);
			connection.Open();
			return connection;
		}
	}
}
