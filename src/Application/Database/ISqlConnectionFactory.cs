﻿using System.Data;

namespace LocalScriptGenerator.Database
{
	public interface ISqlConnectionFactory
	{
		IDbConnection CreateConnection();
	}
}
