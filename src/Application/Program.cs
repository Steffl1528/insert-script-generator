﻿using LocalScriptGenerator.Database;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

Console.WriteLine("START ScriptGenerator");

// the tablenames determine which table will be selected
const string StudentTable = "Student";
const string SchoolTable = "School";

// the categories determine which select statement will be executed
const string StudentCategory = StudentTable;
const string SchoolCategory = SchoolTable;

// the key is the table name
// the value is the category
var tableCategoryDictionary =
	new Dictionary<string, string>
	{
		{ StudentTable, StudentCategory },
		{ SchoolTable, SchoolCategory }
	};

var serviceProvider = SetupApplication.Start();

var dataRepository = serviceProvider.GetService<IDataRepository>()!;

var sbMain =
	new StringBuilder(
		$"---> This Insert Statements were generated automatically, Creation Date and Time: {DateTime.Now} <---\r\n\r\n");


foreach (var (key, value) in tableCategoryDictionary)
{
	// 1. get all column names from all tables
	var tableColumnNames = dataRepository.GetTableColumns(key);

	// 3. get relevant data from all relevant tables
	IEnumerable<dynamic>? tableValues = value switch
	{
		StudentCategory => dataRepository.GetStudentDataFromGivenTable(tableColumnNames, key).ToList(),
		SchoolCategory => dataRepository.GetSchoolDataFromGivenTable(tableColumnNames, key).ToList(),
		_ => default
	};

	if (tableValues == null)
	{
		Console.WriteLine($"The tableValues of table {key} are null");
		throw new Exception();
	}

	// 4. generate the insert statements for all relevant tables
	var generatedInsertStatementResult =
		InsertStatementService.GenerateInsertStatements(key, tableValues, tableColumnNames);

	sbMain.Append(generatedInsertStatementResult);
}

// 5. create the sql file with the the insert statements
FileService.CreateSqlFile(sbMain);

Console.WriteLine("END ScriptGenerator");
Environment.Exit(-1);
