using System.IO;
using System.Text;

public static class FileService
{
	public static void CreateSqlFile(StringBuilder sb)
	{
		using StreamWriter outputFile = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "InsertScript.sql"));
		outputFile.Write(sb.ToString());
	}
}