using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class InsertStatementService
{
	private static readonly string LineBreak = "\r\n";
	public static StringBuilder GenerateInsertStatements(string tableName, IEnumerable<dynamic> dynamicData,
		string[] columnNames)
	{
		var sbMain = new StringBuilder();
		sbMain.AppendLine($"{LineBreak}{LineBreak}-------------------------------------------");
		sbMain.AppendLine($"--->Insert statements for table {tableName}<---");
		sbMain.AppendLine("-------------------------------------------");
		sbMain.Append($"INSERT INTO {tableName} (");
		sbMain.AppendJoin(",", columnNames);
		sbMain.Append($"){LineBreak} VALUES ");

		foreach (var data in dynamicData)
		{
			sbMain.Append("(");
			var fields = data as IDictionary<string, object?>;
			sbMain.AppendJoin(",", fields!.Values.Select(fieldvalue =>
			{
				if (fieldvalue == null)
				{
					return "NULL";
				}

				switch (Type.GetTypeCode(fieldvalue.GetType()))
				{
					case TypeCode.DateTime:
					var sqlValidFormattedDateTime = ((DateTime)fieldvalue).ToString("s");
					return $"'{sqlValidFormattedDateTime}'";
					case TypeCode.String:
					// if column value is DateTime or String, append "'{Value}'"
					// the single quotation marks are important here
					return $"'{fieldvalue}'";
					case TypeCode.Decimal:
					// if column value is Decimal replace the comma "," with a dot "."
					return $"{fieldvalue}".Replace(",", ".");
					default:
					return fieldvalue;
				}
			}));

			if (data != dynamicData.Last())
			{
				sbMain.AppendLine("),");
			}
			else
			{
				sbMain.AppendLine(")");
			}
		}

		return sbMain;
	}
}