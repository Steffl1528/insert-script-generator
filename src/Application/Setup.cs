using LocalScriptGenerator.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

public static class SetupApplication
{
	const string DEVELOPMENT = "development";
	const string TEST = "test";
	const string DATABASE_DEV = "DatabaseDev";
	const string DATABASE_TEST = "DatabaseTest";

	public static ServiceProvider Start()
	{
		var environmentToFetch = DEVELOPMENT;
		if (Environment.GetCommandLineArgs().Length > 1)
		{
			// get arguments
			var environmentArgument = Environment.GetCommandLineArgs()[1];
			environmentToFetch = environmentArgument.ToLower();
		}
		Console.WriteLine("GET Data from " + environmentToFetch);

		var builder = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			// setup the appsettings.json file
			.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
			// setup the user secrets
			// they will get injected into the appsettings.json file
			.AddUserSecrets<ConnectionStrings>();

		var configuration = builder.Build();

		// get connectionstring from appsettings.json
		var connectionString = environmentToFetch switch
		{
			TEST => configuration.GetConnectionString(DATABASE_TEST),
			_ => configuration.GetConnectionString(DATABASE_DEV)
		};

		if (string.IsNullOrEmpty(connectionString))
		{
			throw new NullReferenceException("The connectionstring is null or empty");
		}

		var services = new ServiceCollection();
		services.AddSingleton<ISqlConnectionFactory>(_ =>
			new SqlConnectionFactory(connectionString));
		services.AddTransient<IDataRepository, DataRepository>();

		return services.BuildServiceProvider();
	}

	private class ConnectionStrings
	{
		public string? DatabaseDev { get; set; }
		public string? DatabaseTest { get; set; }
	}
}