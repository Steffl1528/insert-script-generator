# insert-script-generator

## A few Informations before using the application

> This application will not work out of the box.  
> You will have to setup the application according to your needs.

## Getting started

### Configure the code to your needs

In "Program.cs" you can setup all Tables where you need data from.  
You can also specify which select statements will be used.

In DataRepository.cs you have to define the sql queries, that you want to run to get your data.

### Setup and start the application

1 navigate to the application

```shell
cd {YOURRepositoryPath}\src\Application
```

2 Create the secrets for the connection strings

> A secrets.json file should have been created inside the folder "C:\Users\\{YourUserName}\AppData\Roaming\Microsoft\UserSecrets\insert-script-generator" after executing one of the commands

```shell
dotnet user-secrets set "ConnectionStrings:DatabaseDev" "Data Source=<YOURservername>;Initial Catalog=<YOURDatabaseDev>;User id=<YOURusername>;Password=<YOURpassword>;"
```

```shell
dotnet user-secrets set "ConnectionStrings:DatabaseTest" "Data Source=<YOURservername>;Initial Catalog=<YOURDatabaseTest>;User id=<YOURusername>;Password=<YOURpassword>;"
```

3 build the project

```shell
dotnet build
```

4 Generate an insert script  
4.1 for "DatabaseDev"

```shell
dotnet bin/Debug/net7.0/InsertScriptGenerator.dll
```

4.2 for "DatabaseTest"

```shell
dotnet bin/Debug/net7.0/InsertScriptGenerator.dll test
```

5 The insert script should have been created inside the folder

> {YOURRepositoryPath}\src\Application\bin\Debug\net7.0
